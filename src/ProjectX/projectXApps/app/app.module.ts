﻿import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import "hammerjs";
import { MaterialModule } from "@angular/material";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppComponent } from "./app.component";
import { CoreModule } from './core/core.module';


@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        CoreModule.forRoot(),
        MaterialModule
    ],

    declarations: [AppComponent],
    providers: [],
    bootstrap: [AppComponent]
})

export class AppModule { }