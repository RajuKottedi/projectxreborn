﻿// styles
import './styles.scss';

import { platformBrowser } from '@angular/platform-browser';
import { enableProdMode } from '@angular/core';
import { AppModuleNgFactory } from '../aot/projectXApps/app/app.module.ngfactory';

// Entry point for AoT compilation
declare var System: any;

enableProdMode();

platformBrowser().bootstrapModuleFactory(AppModuleNgFactory);
