# Project X

This article shows how [Webpack](http://webpack.github.io/docs/) could be used together with [Visual Studio](https://www.visualstudio.com/) ASP.NET Core and [Angular](https://angular.io/docs/ts/latest/quickstart.html). Both the client and the server side of the application is implemented inside one ASP.NET Core project which makes it easier to deploy.

|                           | Build                                                                                                                                                             |       
| ------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| .NET Core, angular        | [![Build status](https://ci.appveyor.com/api/projects/status/43oae87lx16b1kq7/branch/master?svg=true)](https://ci.appveyor.com/project/RajuKottedi/projectxreborn/branch/master)
